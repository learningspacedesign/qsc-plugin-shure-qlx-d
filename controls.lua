table.insert(ctrls, {
  Name = "IPAddress",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "Status",
  ControlType = "Indicator",
  IndicatorType = "Status",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "UnitFirmware",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "ChannelName",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "DeviceID",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AudioGain",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "Group-Channel",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Both",
})
table.insert(ctrls, {
  Name = "Frequency",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryType",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryLevel",
  ControlType = "Knob",
  ControlUnit = "Integer",
  Min = 0,
  Max = 5,
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "TransmitterType",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "RFPower",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "TransmitterPowerLock",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "TransmitterMenuLock",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "Encryption",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "TransmitterMute",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "UnitMacAddress",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryCycle",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryRunTime",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryTemp",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryCharge",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "BatteryHealth",
  ControlType = "Text",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "RFLevel",
  ControlType = "Knob",
  ControlUnit = "Integer",
  Min = 0,
  Max = 115,
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AudioLevel",
  ControlType = "Knob",
  ControlUnit = "Integer",
  Min = 0,
  Max = 50,
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AntennaA",
  ControlType = "Indicator",
  IndicatorType = "Led",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "AntennaB",
  ControlType = "Indicator",
  IndicatorType = "Led",
  Count = 1,
  UserPin = true,
  PinStyle = "Output",
})
table.insert(ctrls, {
  Name = "EncryptionModeToggle",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  UserPin = true,
  PinStyle = "Both",
})