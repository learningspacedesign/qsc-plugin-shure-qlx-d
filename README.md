# Shure QLX-D Plugin

This QSYS plugin is for control and feedback of the Shure QLX-D wireless reciever. This plugin uses the Shure Command Strings and only supports functions detailed in the Shure documentation.

See [Shure QLX-D Command Strings Documentation](https://d24z4d3zypmncx.cloudfront.net/Pubs/qlx-d_digital_wireless/qlx-d-network-string-commands.pdf)

## Plugin Information

This version was tested against a Shure QLX-D Reciever with a QLXD1 Bodypack and Shure Rechargable Battery running Shure firmware 2.3.24 and was the basis for inital development.

## Overview

### Control Page
![Control Page](./Help%20File/Shure%20QLXD%20Control%20Page.png)

**Function Description**
- Unit Firmware Version (Shows Current Firmware Version)
  - **Read Only**
- Unit MAC Address (Shows Current Network Port MAC Address)
  - **Read Only**
- Channel Name (Allows You to Name the Channel Name That Show in Workbench)
  - **Read,Write**
- Audio Gain (Adjusts Audio Gain On The Output Of The Reciever | 000 To 060 or -18db to 42db)
  - **Read,Write**
- Group, Channel (Allows You to Set the Group and Channel for the Reciever | Must be Comma Separated)
  - **Read,Write**
- Device ID (Shows Current Shure Device ID)
  - **Read Only**
- Frequency (Shows Current Frequency Assigned for Group,Channel Assignment)
  - **Read Only**
- Transmitter Type (Shows Current Shure Transmitter Model)
  - **Read Only**
- RF Power (Shows Current RF Power | High, Low, or Unknown)
  - **Read Only**
- Transmitter Power Lock (Shows Current Status)
  - **Read Only**
- Transmitter Menu Lock (Shows Current Status)
  - **Read Only**
- Transmitter Mute (Shows Current Status)
  - **Read Only**
- Encryption (Shows Current Status)
  - **Read Only**
- Transmitter Power Lock (Shows Current Status)
  - **Read Only**
- Audio Level (Shows Realtime Feedback Of Audio Level From 000 To 050 Sampled Every 300ms)
  - **Read Only**
- RF Level (Shows Realtime Feedback Of RF Level From 000 To 115 Sampled Every 300ms)
  - **Read Only**
- Antenna A & B (Shows Realtime Feedback and Indicates the blue RF LED’s from the receiver. These show the squelch status of the receiver Sampled Every 300ms)
  - **Read Only**

### Battery Page
![Battery Page](./Help%20File/Shure%20QLXD%20Battery%20Page.png)

**Function Description**
- Battery Type (Shows Current Battery Type Installed For Shure Rechargable Battery ONLY | ALKA, LION, LITH, NIMH, UNKN)
  - **Read Only**
- Battery Cycles (Shows Current Fully Charge Cycle Counts For Shure Rechargable Battery ONLY)
  - **Read Only**
- Battery Run Time (Shows Current Time Left on Battery Charge For Shure Rechargable Battery ONLY)
  - **Read Only**
- Battery Temperature (Shows Current Charge Status as a Percent For Shure Rechargable Battery ONLY)
  - **Read Only**
- Battery Health (Shows Current Capacity the Battery Currently has Left Relative to the Factory Defined Orignal Capacity For Shure Rechargable Battery ONLY)
  - **Read Only**
- Battery Level (Shows Current Battery Level Bars | 0 to 5 value for upto 5 bars)
  - **Read Only**

### Setup Page
![Setup Page](./Help%20File/Shure%20QLXD%20Setup%20Page.png)

**Function Description**
- IP Address (Set IP Address Of Shure QLX-D Control Port For Plugin To Connect)
  - **Read,Write**
- Connection Status (Show Current Connection Status Of Plugin)
  - **Read Only**